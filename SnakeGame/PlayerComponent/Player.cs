﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame.PlayerComponent
{
    public class Player : Component
    {
        /// <summary>
        /// 蛇頭的位址
        /// </summary>
        /// <returns></returns>
        public override Point GetSnakeHeadPoint()
        {
            return new Point(0, 0);
        }

        /// <summary>
        /// 取得蛇的長度
        /// </summary>
        public override int GetSnakeLength()
        {
            return 3;
        }
    }
}
