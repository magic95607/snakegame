﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame.PlayerComponent
{
    public class PlayerFactory : IPlayerFactory
    {
        public Player CreatePlayer()
        {
            return new Player();
        }
    }
}
