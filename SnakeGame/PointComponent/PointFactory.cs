﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame.PointComponent
{
    public class PointFactory : IPointFactory
    {
        public PointDecorator CreatePoint(SnakeDirection snakeDirection)
        {
            PointDecorator pointDecorator = new NoneMove();
            switch (snakeDirection)
            {
                case SnakeDirection.Up:
                    {
                        pointDecorator = new UpMove();
                        break;
                    }
                case SnakeDirection.Down:
                    {
                        pointDecorator = new DownMove();
                        break;
                    }
                case SnakeDirection.Left:
                    {
                        pointDecorator = new LeftMove();
                        break;
                    }
                case SnakeDirection.Right:
                    {
                        pointDecorator = new RightMove();
                        break;
                    }

            }
            return pointDecorator;
        }
    }
}
