﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame.PointComponent
{
    public class DownMove : PointDecorator
    {
        public override Point GetSnakeHeadPoint()
        {
            return new Point(base.GetSnakeHeadPoint().X, base.GetSnakeHeadPoint().Y + MovePoint);
        }
    }
}
