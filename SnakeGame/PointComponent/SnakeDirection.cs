﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame.PointComponent
{
    public enum SnakeDirection
    {
        None,
        Up,
        Down,
        Left,
        Right
    }
}
