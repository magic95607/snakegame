﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SnakeGame.FoodComponent;
using SnakeGame.PlayerComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeGame
{
    static class Program
    {
        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            IServiceCollection services = new ServiceCollection();
            services.AddScoped<frmSnake>();
            services.AddScoped<IFoodFactory, FoodFactory>();
            services.AddScoped<IPlayerFactory, PlayerFactory>();
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConsole();
                loggingBuilder.AddDebug();
            });
            var serviceProvider = services.BuildServiceProvider();
            Application.Run(serviceProvider.GetService<frmSnake>());
        }
    }
}
