﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame.FoodComponent
{
    public class GeneralFood : FoodDecorator
    {
        public Point Point { get; set; }

        public override int GetSnakeLength()
        {
            return base.GetSnakeLength() + 1;
        }
    }
}
