﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame.FoodComponent
{
    public abstract class FoodDecorator : Component
    {
        protected Component component;

        /// <summary>
        /// 食物誰被吃掉
        /// </summary>
        /// <param name="component"></param>
        public void SetPlayer(Component component)
        {
            this.component = component;
        }

        /// <summary>
        /// 取得蛇頭的位置
        /// </summary>
        public override Point GetSnakeHeadPoint()
        {
            if (this.component != null)
            {
                return this.component.GetSnakeHeadPoint();
            }
            else
            {
                return new Point(0, 0);
            }
        }

        /// <summary>
        /// 取得蛇的長度
        /// </summary>
        public override int GetSnakeLength()
        {
            if (this.component != null)
            {
                return this.component.GetSnakeLength();
            }
            else
            {
                return 0;
            }
        }
    }
}
