﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame.FoodComponent
{
    public interface IFoodFactory
    {
        FoodDecorator CreateFood();
    }
}
