﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame
{
    public abstract class Component
    {
        /// <summary>
        /// 取得蛇的長度
        /// </summary>
        public abstract int GetSnakeLength();
        /// <summary>
        /// 取得蛇的位置
        /// </summary>
        public abstract Point GetSnakeHeadPoint();
    }
}
