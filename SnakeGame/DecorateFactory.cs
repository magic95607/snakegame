﻿using SnakeGame.FoodComponent;
using SnakeGame.PointComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame
{
    public class DecorateFactory
    {
        Component _original;

        public DecorateFactory(Component original)
        {
            _original = original;
        }

        public DecorateFactory SetDecodeFood(FoodDecorator foodDecorator)
        {
            if (foodDecorator != null)
            {
                foodDecorator.SetPlayer(_original);
                _original = foodDecorator;
            }
            return this;
        }

        public DecorateFactory SetDecodeMove(PointDecorator pointDecorator)
        {
            if (pointDecorator != null)
            {
                pointDecorator.SetPlayer(_original);
                _original = pointDecorator;
            }
            return this;
        }

        public Component GetDecodePlayer()
        {
            return _original;
        }
    }

}
