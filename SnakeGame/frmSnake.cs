﻿using Microsoft.Extensions.Logging;
using SnakeGame.FoodComponent;
using SnakeGame.PlayerComponent;
using SnakeGame.PointComponent;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeGame
{
    public partial class frmSnake : Form
    {
        /// <summary>
        /// 食物工廠
        /// </summary>
        private readonly IFoodFactory _foodFactory;
        /// <summary>
        /// 玩家工廠
        /// </summary>
        private readonly IPlayerFactory _playerFactory;
        /// <summary>
        /// 紀錄者
        /// </summary>
        private readonly ILogger _logger;
        private DecorateFactory _decorateFactory;
        private SnakeDirection _snakeDirection = SnakeDirection.None;
        private Timer _timer;
        public frmSnake(IFoodFactory foodFactory, IPlayerFactory playerFactory, ILogger<frmSnake> logger)
        {
            InitializeComponent();
            _logger = logger;
            _foodFactory = foodFactory;
            _playerFactory = playerFactory;
            _timer = new Timer();
            _timer.Tick += _timer_Tick;
            _timer.Interval = 500;
            _timer.Start();
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.DarkKhaki);
            var food = _foodFactory.CreateFood();
            Pen pen = new Pen(Color.Red, 2);
            SolidBrush brush = new SolidBrush(Color.Green);
            g.DrawRectangle(pen, food.GetSnakeHeadPoint().X, generalFood.Point.Y, 15, 15);
            g.FillRectangle(brush, food.get.X, generalFood.Point.Y, 15, 15);


            IPointFactory pointFactory = new PointFactory();
            var move = pointFactory.CreatePoint(_snakeDirection);
            _decorateFactory.SetDecodeMove(move);

            var snakeLength = _decorateFactory.GetDecodePlayer().GetSnakeLength();
            _logger.LogInformation($"snake length {snakeLength}");

            var snakePoint = _decorateFactory.GetDecodePlayer().GetSnakeHeadPoint();

            Pen pen = new Pen(Color.Blue, 2);
            g.DrawRectangle(pen, snakePoint.X, snakePoint.Y, 15, 15);
            g.FillRectangle(Brushes.Green, snakePoint.X, snakePoint.Y, 15, 15);

            _logger.LogInformation($"snake position x:{snakePoint.X} y:{snakePoint.Y}");
        }

        private void frmSnake_Load(object sender, EventArgs e)
        {
            Component player = _playerFactory.CreatePlayer();
            _decorateFactory = new DecorateFactory(player);
        }

        private void frmSnake_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                _snakeDirection = SnakeDirection.Up;
            }
            else if (e.KeyCode == Keys.Down)
            {
                _snakeDirection = SnakeDirection.Down;
            }
            else if (e.KeyCode == Keys.Right)
            {
                _snakeDirection = SnakeDirection.Right;
            }
            else if (e.KeyCode == Keys.Left)
            {
                _snakeDirection = SnakeDirection.Left;
            }
        }
    }
}
